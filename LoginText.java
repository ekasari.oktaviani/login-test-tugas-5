import javafx.scene.layout.Priority;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginText {
    WebDriver driver;

    @BeforeClass
    public void setup() {
        System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void keluar() throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(100);
    }

    @Test(priority = 0)
    public void TesSatu() {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        String username = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
    }

    //*[@id="profile"]/div/div/div/p[2]/a
    @Test(priority = 1)
    public void TesDua() {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("password salah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(),"Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 2)
    public void Testiga(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("username salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("password");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @Test(priority = 3)
    public void TesEmpat(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");
        //login
        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("username salah");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("password salah");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");
    }

    @AfterClass
    public void closebrowser() throws InterruptedException {
        Thread.sleep(20);
        driver.quit();
    }

}
